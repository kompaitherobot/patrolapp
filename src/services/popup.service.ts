// to send mail and sms alert

import { Injectable, OnInit } from "@angular/core";
import { App } from "ionic-angular";
import { AlertService } from "./alert.service";
import { AlertController, Alert,ToastController} from "ionic-angular";
import { ApiService } from "./api.service";
import { ParamService } from "./param.service";

@Injectable()
export class PopupService implements OnInit {
  alert_person: Alert;
  alert_blocked: Alert;
  private accesspic: () => void;
  private accessparam: () => void;
  private addMail: (ev) => void;
  private deleteHistory: () => void;
  private accessautolaunch: () => void;
  onSomethingHappened1(fn : () => void) {
    this.accessparam = fn;
  }
  onSomethingHappened2(fn : () => void) {
    this.addMail = fn;
  }
  onSomethingHappened3(fn : () => void) {
    this.deleteHistory = fn;
  }
  onSomethingHappened4(fn : () => void) {
    this.accessautolaunch = fn;
  }
  onSomethingHappened5(fn : () => void) {
    this.accesspic = fn;
  }
  constructor(
    public app: App,
    public alert: AlertService,
    public param: ParamService,
    public api: ApiService,
    private alertCtrl: AlertController,
    public toastCtrl:ToastController
  ) {}

  ngOnInit() {}

  relocAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.relocAlert_title,
      message: this.param.datatext.relocAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
            window.close();
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  startFailedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.error,
      message: this.param.datatext.cantstart,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
            window.close();
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  presentAlert() {
    // pop up if the robot is in the docking when you start the round
    let alert = this.alertCtrl.create({
      title: this.param.datatext.presentAlert_title,
      message: this.param.datatext.presentAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          cssClass: "cancelpopup",
          role: "cancel",
          handler: () => {
            console.log("cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Yes clicked");
            this.api.roundActive = true;
            this.api.fct_startRound = true;
            setTimeout(() => {
              this.alert.roundLaunched(this.api.mailAddInformationRound());
            }, 3000);
          },
        },
      ],
    });

    alert.present();
  }

  presentConfirm() {
    // pop up code to resume or stop the round
    let alert = this.alertCtrl.create({
      title: this.param.datatext.presentConfirm_title,
      message: this.param.datatext.presentConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            console.log("Non clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Oui clicked");
            this.api.fct_onGo = true;
          },
        },
      ],
    });
    alert.present();
  }

  FallConfirm() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.FallConfirm_title,
      message: this.param.datatext.FallConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
            this.api.popupFall=false;
           
          },
        },
      ],
    });
    alert.present();
  }

  RemoteConfirm() {
    // pop up code to resume or stop the round
    let alert = this.alertCtrl.create({
      title: this.param.datatext.RemoteConfirm_title,
      message: this.param.datatext.RemoteConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            console.log("No clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Yes clicked");
            this.api.fct_onGo = true;
          },
        },
      ],
    });
    alert.present();
  }

  blockedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.blockedAlert_title,
      message: this.param.datatext.blockedAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  StartBlockedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.startblockedAlert_title,
      message: this.param.datatext.startblockedAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }
  
  goDockingConfirm() {
    // pop up to ask if the robot must go to the docking
    let alert = this.alertCtrl.create({
      title:
        this.param.datatext.goDockingConfirm_title +
        this.api.battery_status.remaining +
        "%",
      message: this.param.datatext.goDockingConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            //console.log('Non clicked');
            this.api.towardDocking = false;
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            //console.log('Oui clicked');
            this.api.towardDocking = true;
            this.api.reachHttp("docking");
            this.alert.askCharge(this.api.mailAddInformationBasic());
          },
        },
      ],
    });
    alert.present();
  }

  lowBattery() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.lowBattery_title,
      message: this.param.datatext.lowBattery_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            //console.log('Non clicked');
            this.api.towardDocking = false;
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Oui clicked");
            this.api.towardDocking = true;
            //console.log(this.api.towardDocking=true);
            this.api.reachHttp("docking");
            this.alert.askCharge(this.api.mailAddInformationBasic());
          },
        },
      ],
    });
    alert.present();
  }

  errorlaunchAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.errorlaunchAlert_title,
      message: this.param.datatext.errorlaunchAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  robotmuststayondocking() {
    // pop up if the robot is in the docking when you start the round
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.robotmuststayondocking_title,
      message: this.param.datatext.robotmuststayondocking_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  errorNavAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.errorNavAlert_title,
      message: this.param.datatext.errorNavAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  lostAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.lostAlert_title,
      message: this.param.datatext.lostAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  quitConfirm() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.quitConfirm_title,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            //console.log('Non clicked');
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            //console.log('Oui clicked');
            this.api.close_app = true;
            this.alert.appClosed(this.api.mailAddInformationBasic());
            //stop the round and quit the app
            this.api.abortHttp();
            this.api.deleteEyesHttp(23);
            setTimeout(() => {
              window.close();
            }, 1000);
          },
        },
      ],
    });
    alert.present();
  }

  errorBlocked() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.errorBlocked_title,
      message: this.param.datatext.errorBlocked_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('OK clicked');
          },
        },
      ],
    });
    alert.present();
  }

  statusRedPresent() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.statusRedPresent_title,
      message: this.param.datatext.statusRedPresent_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  statusGreenPresent() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.statusGreenPresent_title,
      message: this.param.datatext.statusGreenPresent_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  leaveDockingConfirm() {
    // pop up to ask if the robot must leave the docking
    let alert = this.alertCtrl.create({
      title:
        this.param.datatext.leaveDockingConfirm_title +
        this.api.battery_status.remaining +
        "%",
      message: this.param.datatext.leaveDockingConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            console.log("Non clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Oui clicked");
            this.alert.leaveDocking(this.api.mailAddInformationBasic());
            this.api.disconnectHttp();
          },
        },
      ],
    });
    alert.present();
  }

  PersonConfirm() {
    this.alert_person = this.alertCtrl.create({
      title: this.param.datatext.AlertPersonConfirm,
      message: this.param.datatext.personConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
            this.api.popupPerson=false;
          },
        },
      ],
    });
    this.alert_person.present();
  }
  
  askpswd(pageName:string) {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.enterPassword,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password) ) {
              if(pageName == "autolaunch"){
                this.accessautolaunch();
              }
              else if(pageName == "seepic"){
                this.accesspic();
              }
              else{
                this.accessparam();
              }
                
            } else {
              this.wrongPassword(pageName);
            }
          },
        },
      ],
    });
    alert.present();
  }

  emptyHistory(ev) {
    ev.preventDefault
    let alert = this.alertCtrl.create({
      title: this.param.datatext.emptyHistory,
      message: this.param.datatext.confirmHistoryEmptying,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          role: "backdrop",
          handler: () => {
            this.deleteHistory();
          },
        },
      ],
    });
    alert.present();
  }

  wrongPassword(pageName:string) {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.wrongPassword,
      cssClass: "alertstyle_wrongpass",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password)){
              if(pageName == "autolaunch"){
                this.accessautolaunch();
              }
              else if(pageName == "seepic"){
                this.accesspic();
              }  
              else{
                this.accessparam();
              }
                
            } else {
              this.wrongPassword(pageName);
            }
          },
        },
      ],
    });
    alert.present();
  }


  displayrgpd(ev) {
    ev.preventDefault();
    let alert = this.alertCtrl.create({
      title: "RGPD",
      message: this.param.datatext.rgpd_txt,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
     
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
              this.addMail(ev);
            }
          },
        
      ],
    });
    alert.present();
  }

  
async showToast(msg,duration,position) {
  const toast = await this.toastCtrl.create({
    message: msg,
    duration: duration,
    position: position,//top, middle, bottom
    cssClass:"toastok"
  });
  toast.present();
}

  
async showToastRed(msg,duration,position) {
  const toast = await this.toastCtrl.create({
    message: msg,
    duration: duration,
    position: position,//top, middle, bottom
    cssClass:"toast"
  });
  toast.present();
}

}
