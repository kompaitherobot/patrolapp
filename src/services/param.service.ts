// the parameter script

import frtext from "../langage/fr-FR.json";
import entext from "../langage/en-GB.json";
import estext from "../langage/es-ES.json";
import detext from "../langage/de-DE.json";
import ittext from "../langage/it-IT.json";
import grtext from "../langage/el-GR.json";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";
import { cloneDeep } from 'lodash';

@Injectable()
export class ParamService {
  localhost: string;
  robotmail: string;
  datamail: string;
  langage: string;
  phonenumberlist: string[];
  maillist: string[];
  datamaillist: string[];
  serialnumber: string;
  mailrobotpassw: string;
  maildatapassw: string;
  allowspeech: boolean;
  name: string;
  datatext: any;
  robot: any;
  reservations:any;
  duration: any;
  durationNS:any={};
  battery:any;
  tableduration: any = {};
  tabledurationNS: any = {};
  tablerobot: any = {};
  tablephotohistory: any = {};
  tablepatrol: any = {};
  tablereservation: any = {};
  mail: any = {};
  phone: any = {};
  date: any;
  currentduration: any = {};
  cpt: number;
 
  sendduration: boolean;
  source: any;
  autolaunch:boolean=false;
  photos: any = [];
  patrolInfo: any = {};
  missionsList: any = [];
  missionsDisplay: any;
  missionsDisplayToday: any;

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
    this.maillist = [];
    this.phonenumberlist = [];
    this.cpt = 0;
    this.sendduration = false;
  }

  addDuration() {
    this.tableduration.action = "insert";
    this.tableduration.serialnumber = this.serialnumber;
    this.tableduration.round = this.currentduration.round;
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.walk = this.currentduration.walk;
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addduration.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addMission(mission:any){
    this.tablereservation.action = "insert";
    this.tablereservation.date = mission.date.substring(0,10) + " " + mission.date.substring(11,19);
    this.tablereservation.idRound = mission.idRound;
    this.tablereservation.type = mission.type;
    this.tablereservation.tpsReserv = mission.tpsReserv;
    this.tablereservation.idApp = 3;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addreservation.php",
        JSON.stringify(this.tablereservation)
      )
      .subscribe(
        (data) => {
          console.log(data);
          
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDurationNS(){
    this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(
      (data) => {
        this.duration = data;
        this.durationNS = data;
        console.log(data);
        console.log(this.durationNS.length);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateDurationNS(id:number){
    

    this.tabledurationNS.action="update";
    this.tabledurationNS.id_duration=id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updateduration.php",
        JSON.stringify( this.tabledurationNS)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }


  updateDuration() {
    this.tableduration.action = "update";
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updatedurationpatrol.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDataRobot() {
    this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(
      (data) => {
        //console.log("robot:"+data);
        this.robot = data[0];
        this.langage = this.robot.langage;
        if (this.langage === "fr-FR") {
          this.datatext = frtext;
        } else if (this.langage === "en-GB") {
          this.datatext = entext;
        } else if (this.langage === "es-ES") {
          this.datatext = estext;
        }else if (this.langage === "de-DE") {
          this.datatext = detext;
        }else if(this.langage === "el-GR"){
          this.datatext = grtext;
        }else if(this.langage === "it-IT"){
          this.datatext = ittext;
        }
    
        this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_patrolapp)
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getPhone() {
    this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(
      (data) => {
        if (this.phonenumberlist.length === 0) {
          for (let value in data) {
            this.phonenumberlist.push(data[value].phonenumber);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataPhoto() {
    this.httpClient
      .get("http://localhost/ionicDB/getphoto.php")
      .subscribe(
        (data) => {
          console.log(data);
          this.photos = data;
          this.photos.forEach(photo => {
            var date = photo.date_photo;
            photo.date_photo = new Date(date).toLocaleDateString(this.langage);
            photo['time_photo'] = new Date(date).toLocaleTimeString(this.langage, {hour: '2-digit', minute:'2-digit'});
          });
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDataReservationPatrol(roundList:any) {
    this.httpClient
      .get("http://localhost/ionicDB/getreservationpatrol.php")
      .subscribe(
        (data) => {
          //console.log(data);
          this.missionsList = data;
          this.missionsList.forEach(mission => {
            var date = mission.date
            mission.date = new Date(date.substring(0,10) + "T" + date.substring(11,date.length));
          });
          this.updateDisplayMission(roundList);
        },
        (err) => {
          console.log(err);
        }
      )
  }

  getDataPatrol() {
    this.httpClient
      .get("http://localhost/ionicDB/getpatrol.php")
      .subscribe(
        (data) => {
          //console.log(data);
          this.patrolInfo = data[0];
          
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addMail(m: string) {
    this.mail.action = "insert";
    this.mail.mail = m;
    this.mail.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addPhoto(url: string){
    this.tablephotohistory.action = "insert";
    this.tablephotohistory.url = url;
    this.httpClient.post(
      "http://localhost/ionicDB/addphoto.php",
      JSON.stringify(this.tablephotohistory)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updateRobot() {
    this.tablerobot.action = "update";
    this.tablerobot.langage = this.robot.langage;
    this.tablerobot.serialnumber = this.serialnumber;
    this.tablerobot.name = this.name;
    this.tablerobot.send_pic = this.robot.send_pic;
    this.tablerobot.password=this.robot.password;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updaterobotpassword.php",
        JSON.stringify(this.tablerobot)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updatePatrol() {
    this.tablepatrol.action = "update";
    this.tablepatrol.patrol_id = this.patrolInfo.patrol_id;
    this.tablepatrol.photo_history = this.patrolInfo.photo_history;
    this.tablepatrol.person_detection = this.patrolInfo.person_detection;
    this.tablepatrol.fall_detection = this.patrolInfo.fall_detection;
    this.tablepatrol.detection_stop=this.patrolInfo.detection_stop;
    this.tablepatrol.mask_detection=this.patrolInfo.mask_detection;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updatepatrolmask.php",
        JSON.stringify(this.tablepatrol)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }


  deleteMail(m: string) {
    this.mail.action = "delete";
    this.mail.mail = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletemail.php",
        JSON.stringify(this.mail)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deletePhotoHistory() {
    this.tablephotohistory.action = "delete";
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletephotohistory.php",
        JSON.stringify(this.tablephotohistory)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteMission(id:number){
    this.tablereservation.action = "delete"
    this.tablereservation.id = id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletereservation.php",
        JSON.stringify(this.tablereservation)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updateDisplayMission(roundList:any){
    this.missionsDisplay = cloneDeep(this.missionsList);
    this.missionsDisplay.forEach(mission => {
        mission.name = this.getRoundName(mission.idRound,roundList);
        if(mission.name == undefined){
          this.deleteMission(mission.id);
          this.getDataReservationPatrol(roundList);
        } else {
          var date = mission.date.toLocaleDateString(this.langage);
          var time = mission.date.toLocaleTimeString(this.langage,{hour: '2-digit', minute:'2-digit'});
          mission.date = date;
          mission.time = time;
          mission.type = this.getReservationType(mission);
        }
    });
    this.missionsDisplayToday = this.missionsDisplay.filter(
      (mission) => mission.date == new Date().toLocaleDateString(this.langage)
    );
    //console.log(this.missionsDisplay);
  }


  getBattery(){
    this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(
      (data) => {
        this.battery = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addPhone(m: string) {
    this.phone.action = "insert";
    this.phone.phonenumber = m;
    this.phone.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deletePhone(m: string) {
    this.phone.action = "delete";
    this.phone.phonenumber = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletephone.php",
        JSON.stringify(this.phone)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getMail() {
    this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(
      (data) => {
        if (this.maillist.length === 0) {
          for (let value in data) {
            this.maillist.push(data[value].mail);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getreservations() {
    
    this.httpClient.get("http://localhost/ionicDB/autolaunch/getreservations.php").subscribe(
      (data) => {
          this.reservations=data[0];
          console.log(this.reservations);
      },
      (err) => {
        console.log(err);
      }
    );
  }


  fillData() {
    this.name = this.robot.name;
    this.allowspeech = this.robot.allowspeech;
    this.localhost = this.robot.localhost;
    this.serialnumber = this.robot.serialnumber;
    this.robotmail = this.robot.mailrobot;
    this.datamail = this.robot.maildata;
    this.maildatapassw = atob(this.robot.maildatapass);
    this.mailrobotpassw = atob(this.robot.mailrobotpass);
    this.datamaillist = ["data@kompai.com"];
    if(this.duration.length>0){
    if (
      this.duration[this.duration.length - 1].date ===
      new Date().toLocaleDateString("fr-CA")
    ) {
      this.currentduration.date = this.duration[this.duration.length - 1].date;
      this.currentduration.round = this.duration[
        this.duration.length - 1
      ].round;
      this.currentduration.battery = this.duration[
        this.duration.length - 1
      ].battery;
      this.currentduration.patrol = this.duration[
        this.duration.length - 1
      ].patrol;
      this.currentduration.walk = this.duration[this.duration.length - 1].walk;
      this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
      this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
    } else {
      if(!this.sendduration){
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }
      
    }}else{
      if(!this.sendduration){
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }
    }
  }

  cptDuration() {
    this.autolaunch=false;
    if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
      this.cpt = parseInt(this.currentduration.patrol);
      this.currentduration.patrol = this.cpt + 2;
      this.updateDuration();
    } else {
      this.init_currentduration();
      this.addDuration();
    }
  }


  init_currentduration(){
    this.currentduration.round = 0;
    this.currentduration.battery = 0;
    this.currentduration.patrol = 0;
    this.currentduration.walk = 0;
    this.currentduration.toolbox = 0;
    this.currentduration.logistic = 0;
    this.currentduration.date = new Date().toLocaleDateString("fr-CA");
  }

  getRoundName(id:string, roundList:any){
    var round = roundList.filter(
      (round) => round.Id == parseInt(id)
    );
    var name;
    if(round.length > 0)
      name = round[0].Name;
    else{
      name = undefined;
    }
    return name;
  }

  getReservationType(round:any){
    var type;
    if(round.type == 1)
      type = "x 1";
    else if(round.type == 2)
      type = "x " + this.datatext.dailyLetter;
    else
      type = "x " + this.datatext.weeklyLetter;
    return type;
  }
}
