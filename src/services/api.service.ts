// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, OnInit} from "@angular/core";
import { App } from "ionic-angular";
import { AlertService } from "./alert.service";
import { ParamService } from "./param.service";
declare var ROSLIB: any;

export class Battery {
  autonomy: number; //the estimated remaining operation time, in minutes.
  current: number; //the actual current drawn from the battery, in Amps.
  remaining: number; //the percentage of energy remaining in the battery, range 0 - 100 %.
  status: number; // Charging 0 Chrged 1 Ok 2 Critical 3
  timestamp: number;
  voltage: number; // the actual battery voltage in Volts.
}

export class Statistics {
  timestamp: number;
  totalTime: number;
  totalDistance: number;
}

export class Anticollision {
  timestamp: number;
  enabled: boolean;
  locked: boolean;
  forward: number; // 0 ok , 1 speed low , 2 obstacle
  reverse: number;
  left: number;
  right: number;
}

export class Iostate {
  // receive robot's boutons input
  timestamp: number;
  dIn: any;
  aIn: any;
}

export class Docking {
  status: number;
  detected: boolean;
}
/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/

export class Round {
  round: any;
  acknowledge: boolean; // send to the next poi
  abort: boolean; // stop round
  pause: boolean; // pause the round
  status: number; //0none   1ready to be executed  2moving  3paused  4onPOI  5Completed
}

export class Differential {
  status: number;
}

export class Localization {
  positionx: number;
  positiony: number;
  positiont: number;
}

export class Navigation {
  avoided: number;
  status: number;
}

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5 - Error: indicates that the robot has encountered an error that prevented it to join the required destination.
// This can be cleared by sending a new destination.

@Injectable()
export class ApiService implements OnInit {
  batteryremaining: number;

  battery_status: Battery;
  battery_socket: WebSocket;

  navigation_status: Navigation;
  navigation_socket: WebSocket;

  round_status: Round;
  round_socket: WebSocket;

  anticollision_status: Anticollision;
  anticollision_socket: WebSocket;

  statistics_socket: WebSocket;
  statistics_status: Statistics;

  io_socket: WebSocket;
  io_status: Iostate;

  docking_socket: WebSocket;
  docking_status: Docking;

  differential_status: Differential;
  differential_socket: WebSocket;

  localization_status: Localization;
  localization_socket: WebSocket;
  hour: any;
  textHeadBand: string;
  appOpened: boolean;
  hourStart: Date;
  start: boolean;
  b_pressed: boolean;
  btnPush: boolean;
  towardDocking: boolean;
  towardPOI: boolean;
  close_app: boolean;
  is_connected: boolean;
  is_background: boolean;
  is_localized: boolean;
  is_blocked: boolean;
  roundActive: boolean;
  cpt_jetsonOK: number;
  wifiok: boolean;
  statusRobot: number; //0 ok,  1 jetson without internet,  2 no connexion with jetson
  //mapLoaded:boolean;
  fct_onGo: boolean;
  is_high: boolean;
  fct_startRound: boolean;
  id_current_map: number;
  all_maps: any = [];
  mapdata: any;
  rounddata: any;
  round_current_map: any;
  round_map_selected: any;
  batteryState: number;
  robotok: boolean;
  sos_pressed:boolean;

  falldetected:boolean=false;
  persondetected:boolean=false;
  facedetected:boolean=false;
  maskdetected:boolean=false;

  popupPerson:boolean=false;
  popupFall:boolean=false;

  pauselookperson:number=20;
  pauselookfall:number=20;
  stoplookPerson:number =0;
  stoplookFall:number =0;

  personinterv: any;
  fallinterv: any;

  img_detection:any;
  img_fall:any;

  addMission: boolean;
  socketok:boolean=false;
  ros:any;
  listener_mask:any;
  listener_person:any;
  listener_fall:any;
  pdetected:any;
  fdetected:any;
  all_locations: any;

  httpskomnav: string;
  wsskomnav: string;
  httpsros: string;
  wssros: string;

  constructor(
    private httpClient: HttpClient,
    public app: App,
    public alert: AlertService,
    public param: ParamService
  ) {
    this.is_connected = false; // to know if we are connected to the robot
    this.is_background = false;
    //this.mapLoaded=false;
    this.is_localized = false;
    this.roundActive = false; // to know if the round is in process
    this.is_blocked = false;
    this.towardDocking = false; //to know if the robot is moving toward the docking
    this.towardPOI = false; //to know if th robot is moving toward a poi for the sentinel mode
    this.start = false;
    this.fct_startRound = false;
    this.fct_onGo = false;
    this.statusRobot = 0;
    this.cpt_jetsonOK = 0;
    this.close_app = false;
    this.appOpened = false;
    this.is_high = false;
    this.robotok = false;
    this.sos_pressed=false;
    this.addMission=false;
  }

  stopLookPerson(){//si une personne est detectée on arrete la detection de personne pendant pauselookperson secondes
    this.stoplookPerson=this.stoplookPerson+1;
    //console.log(this.stoplookPerson);
    if(this.stoplookPerson>this.pauselookperson){
      clearInterval(this.personinterv);
      this.stoplookPerson=0;
      
    }
  }

  stopLookFall(){ //si une chute est detectée on arrete la detection pendant pauselookperson secondes
    this.stoplookFall=this.stoplookFall+1;
    //console.log(this.stoplookPerson);
    if(this.stoplookFall>this.pauselookperson){ 
      clearInterval(this.fallinterv);
      this.stoplookFall=0;
     
    }
  }

  instanciate() {
    this.socketok=true;
    var apiserv = this;

    //this.startDetectionHttp();
  
    ////////////////////// localization socket

    this.localization_status = new Localization();

    this.localization_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/localization/socket",
      "json"
    );

    this.localization_socket.onerror = function (event) {
      console.log("error localization socket");
    };

    this.localization_socket.onopen = function (event) {
      console.log("localization socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.localization_status.positionx = test["Pose"]["X"];
        apiserv.localization_status.positiony = test["Pose"]["Y"];
        apiserv.localization_status.positiont = test["Pose"]["T"];
      };

      this.onclose = function () {
        console.log("localization socket closed");
      };
    };

    ////////////////////// docking socket

    this.docking_status = new Docking();

    this.docking_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/docking/socket",
      "json"
    );

    this.docking_socket.onerror = function (event) {
      console.log("error docking socket");
    };

    this.docking_socket.onopen = function (event) {
      console.log("docking socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.docking_status.status = test["Status"];
        apiserv.docking_status.detected = test["Detected"];
      };

      this.onclose = function () {
        console.log("docking socket closed");
      };
    };

    ////////////////////// differential socket

    this.differential_status = new Differential();

    this.differential_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/differential/socket",
      "json"
    );

    this.differential_socket.onerror = function (event) {
      console.log("error differential socket");
    };

    this.differential_socket.onopen = function (event) {
      console.log("differential socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.differential_status.status = test["Status"];
      };

      this.onclose = function () {
        console.log("differential socket closed");
      };
    };

    ////////////////////// navigation socket

    this.navigation_status = new Navigation();

    this.navigation_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/navigation/socket",
      "json"
    );

    this.navigation_socket.onerror = function (event) {
      console.log("error navigation socket");
    };

    this.navigation_socket.onopen = function (event) {
      console.log("navigation socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.navigation_status.status = test["Status"];
        apiserv.navigation_status.avoided = test["Avoided"];
      };

      this.onclose = function () {
        console.log("navigation socket closed");
      };
    };

    ////////////////////// anticollision socket

    this.anticollision_status = new Anticollision();

    this.anticollision_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/anticollision/socket",
      "json"
    );

    this.anticollision_socket.onerror = function (event) {
      console.log("error anticollision socket");
    };

    this.anticollision_socket.onopen = function (event) {
      console.log("anticollision socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.anticollision_status.timestamp = test["Timestamp"];
        apiserv.anticollision_status.enabled = test["Enabled"];
        apiserv.anticollision_status.locked = test["Locked"];
        apiserv.anticollision_status.forward = test["Forward"];
        apiserv.anticollision_status.right = test["Right"];
        apiserv.anticollision_status.left = test["Left"];
      };

      this.onclose = function () {
        console.log("anticollision socket closed");
      };
    };

    ////////////////////// statistics socket

    this.statistics_status = new Statistics();

    this.statistics_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/statistics/socket",
      "json"
    );

    this.statistics_socket.onerror = function (event) {
      console.log("error statistic socket");
    };

    this.statistics_socket.onopen = function (event) {
      console.log("statistic socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.statistics_status.totalTime = test["TotalTime"];
        apiserv.statistics_status.totalDistance = test["TotalDistance"];
        apiserv.statistics_status.timestamp = test["Timestamp"];
      };

      this.onclose = function () {
        console.log("statistic socket closed");
      };
    };

    ///////////////////////// battery socket
    this.battery_status = new Battery();

    this.battery_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/battery/socket",
      "json"
    );

    this.battery_socket.onerror = function (event) {
      console.log("error battery socket");
    };

    this.battery_socket.onopen = function (event) {
      console.log("battery socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.battery_status.autonomy = test["Autonomy"];
        apiserv.battery_status.current = test["Current"];
        apiserv.battery_status.remaining = test["Remaining"];
        apiserv.battery_status.status = test["Status"];
        apiserv.battery_status.timestamp = test["Timestamp"];
        apiserv.battery_status.voltage = test["Voltage"];
      };

      this.onclose = function () {
        console.log("battery socket closed");
      };
    };

    //////////////////////// round socket
    this.round_status = new Round();

    this.round_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/rounds/socket",
      "json"
    );

    this.round_socket.onerror = function (event) {
      console.log("error round socket");
    };

    this.round_socket.onopen = function (event) {
      console.log("round socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.round_status.round = test["Round"];
        apiserv.round_status.acknowledge = test["Acknowledge"];
        apiserv.round_status.abort = test["Abort"];
        apiserv.round_status.pause = test["Pause"];
        apiserv.round_status.status = test["Status"];
      };

      this.onclose = function () {
        console.log("round socket closed");
      };
    };

    //////////////////////////////// io socket
    this.b_pressed = false;
    this.btnPush = false;
    this.io_status = new Iostate();
    this.io_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/io/socket",
      "json"
    );
    this.io_socket.onerror = function (event) {
      console.log("error iosocket");
    };

    this.io_socket.onopen = function (event) {
      console.log("io socket opened");

      this.onclose = function () {
        console.log("io socket closed");
      };
    };

    this.io_socket.onmessage = function (event) {
      var test = JSON.parse(event.data);
      apiserv.io_status.timestamp = test["Timestamp"];
      apiserv.io_status.dIn = test["DIn"];
      apiserv.io_status.aIn = test["AIn"];

      // if(apiserv.io_status.dIn[13] && apiserv.stoplookFall===0 && apiserv.param.patrolInfo.fall_detection==1){
      //   console.log("fall");
      //   apiserv.stoplookFall=1;
      //   apiserv.falldetected=true;
      //   if(apiserv.stoplookPerson>0){
      //     apiserv.stoplookPerson=1;
      //   }else{
      //     apiserv.personinterv = setInterval(() => apiserv.stopLookPerson(), 1000);
      //   }
      //   apiserv.fallinterv = setInterval(() => apiserv.stopLookFall(), 1000);
      // }
      // else if(apiserv.io_status.dIn[12] && apiserv.stoplookPerson===0 && apiserv.param.patrolInfo.person_detection==1){
      //   apiserv.stoplookPerson=1;
      //   apiserv.persondetected=true;
        
      //   apiserv.personinterv = setInterval(() => apiserv.stopLookPerson(), 1000);
      // }

      if (
        apiserv.io_status.dIn[2] || // btn barre
        apiserv.io_status.dIn[0] ||// btn backtab
        apiserv.io_status.dIn[6] || //sos
        apiserv.io_status.dIn[3] ||
        apiserv.io_status.dIn[8]
      ) {

        if(apiserv.io_status.dIn[6]){
          apiserv.sos_pressed=true;
        }
        //smart button and button A of the game pad
        if (!apiserv.b_pressed) {
          apiserv.btnPush = true;
          apiserv.b_pressed = true;
        }
      } else {
        if (apiserv.b_pressed) {
          apiserv.b_pressed = false;
        }
      }
    };

    //////////////////////////////// ROS TOPIC connection
    this.ros = new ROSLIB.Ros({
    url : this.wssros+this.param.robot.rosip
    });

    this.ros.on('connection', function() {
      console.log('Connected to ros websocket server.');
    });

    this.ros.on('error', function(error) {
      console.log('Error connecting to ros websocket server: ', error);
    });

    this.ros.on('close', function() {
      console.log('Connection to ros websocket server closed.');
    });

    //////////////////////////////// ROS TOPIC mask
    this.listener_mask = new ROSLIB.Topic({
      ros : this.ros,
      name : '/mask_detection/status',
      messageType : 'std_msgs/Bool'
    });

    // var listener_face = new ROSLIB.Topic({
    //   ros : this.ros,
    //   name : '/face_detection/status',
    //   messageType : 'std_msgs/Bool'
    // });
    // listener_face.subscribe((message) => {
    //   console.log('Received message on ' + listener_face.name + ': ' + message.data);
    //   this.facedetected=message.data;
      
    // });

    //////////////////////////////// ROS TOPIC person and fall 
    
    this.listener_person = new ROSLIB.Topic({
        ros : this.ros,
        name : '/people_detection/status',
        messageType : 'std_msgs/Bool'
      });
    
    this.listener_fall = new ROSLIB.Topic({
      ros : this.ros,
      name : '/fall_detection/status',
      messageType : 'std_msgs/Bool'
    });
  
  }

  ngOnInit() {}


  mailAddInformationBasic() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 3" +
      "<br>"
    );
  }

  mailAddInformationRound() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    var info;

    if (this.round_status.round && this.round_status.round["Locations"].length>0) {
      info =
        "<br> Round id : " +
        this.round_status.round["Id"] +
        "<br> Round name : " +
        this.round_status.round["Name"] +
        "<br> Going to POI : " +
        this.round_status.round["Locations"][0]["Location"]["Name"];
    } else {
      info =
        "<br> Round id : undefined" +
        "<br> Round name : undefined" +
        "<br> Going to POI : undefined";
    }
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 3" +
      "<br> Round state : " +
      this.round_status.status +
      info
    );
  }

  checkrobot() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
      .subscribe(
        (data) => {
          this.robotok = true;
        },
        (err) => {
          console.log(err);
          this.robotok = false;
        }
      );
  }


  jetsonOK() {
    this.checkInternet();
    this.alert.checkwifi(this.wifiok);
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          if (this.statusRobot === 2) {
            this.deleteEyesHttp(23);
            this.alert.appError(this.mailAddInformationBasic());
            document.location.reload(); //if error then refresh the page and relaunch websocket
          }
          if (resp.status === 200) {
            this.cpt_jetsonOK = 0;
            this.statusRobot = 0; // everything is ok
            this.connect();
          } else {
            this.cpt_jetsonOK += 1;
            if (this.cpt_jetsonOK > 5) {
              this.statusRobot = 2; //no connection
              this.connectionLost();
            }
          }
        },
        (err) => {
          console.log(err); //no conection
          this.cpt_jetsonOK += 1;
          if (this.cpt_jetsonOK > 10) {
            this.statusRobot = 2;
            this.connectionLost();
          }
        }
      );
  }

  joystickHttp(lin, rad) {
    var cmd = { Enable: true, TargetLinearSpeed: lin, TargetAngularSpeed: rad };

    let body = JSON.stringify(cmd);
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    return new Promise((resolve) => {
      this.httpClient
        .put(
          this.httpskomnav + this.param.localhost + "/api/differential/command",
          body,
          options
        )
        .subscribe(
          (resp) => {
            //console.log(resp);
            console.log("joystickOK");
          },
          (error) => {
            console.log(error);
            console.log("joystickPBM");
          }
        );
    });
  }

  pauseHttp(isblocked: boolean) {
    // suspend the round
    //var t0 = performance.now()
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/pause", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          //var t1 = performance.now()
          //this.okToast("Took " + (t1 - t0) + " ms.");
          console.log(resp);
          console.log("pauseOK");
          if (!isblocked) {
            this.roundActive = false;
          }
        },
        (err) => {
          console.log(err);
          console.log("pausePBM");
        }
      );
  }

  connectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("connectOK");
        },
        (err) => {
          console.log(err);
          console.log("connectPBM");
        }
      );
  }

  disconnectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("disconnectOK");
        },
        (err) => {
          console.log(err);
          console.log("disconnectPBM");
        }
      );
  }

  abortDockingHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortdockingOK");
        },
        (err) => {
          console.log(err);
          console.log("abortdockingPBM");
        }
      );
  }

  reachHttp(poiname: string) {
    this.httpClient
      .put(
        this.httpskomnav +
          this.param.localhost +
          "/api/navigation/destination/name/" +
          poiname,
        { observe: "response" }
      )
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("reachOK");
        },
        (err) => {
          console.log(err);
          console.log("reachPBM");
        }
      );
  }

  abortHttp() {
    // stop the round
    this.start = false;
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/abort", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortOK");
        },
        (err) => {
          console.log(err);
          console.log("abortPBM");
        }
      );
  }

  abortNavHttp() {
    // stop the navigation
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortNavOK");
        },
        (err) => {
          console.log(err);
          console.log("abortNavPBM");
        }
      );
  }

  resumeHttp() {
    // resume round

    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/resume", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("resumeOK");
        },
        (err) => {
          console.log(err);
          console.log("resumePBM");
        }
      );
  }

  acknowledgeHttp() {
    //go to next poi

    this.httpClient
      .put(
        this.httpskomnav + this.param.localhost + "/api/rounds/current/acknowledge",
        { observe: "response" }
      )
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("acknowledgeOK");
        },
        (err) => {
          console.log(err);
          console.log("acknowledgePBM");
        }
      );
  }

  getCurrentMap() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
      .subscribe(
        (data) => {
          //console.log(data);
          this.mapdata = data;
          //console.log(this.mapdata.Id);
          if (this.mapdata) {
            this.id_current_map = this.mapdata.Id;
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  async isConnectedInternet(): Promise<boolean> {
    try {
      const response = await fetch('http://localhost/ionicDB/internetping.php');
      const text = await response.text(); // Récupère le contenu texte de la réponse
      //console.log(text);
      
      // Analyse du texte pour déterminer la connectivité Internet
      return text.trim() === 'true'; // Renvoie true si le texte est 'true', sinon false
      
    } catch (error) {
      console.error('Error checking internet connectivity:', error);
      return false;
    }
  }
  checkInternet() {
    this.isConnectedInternet().then(connecte => {
      if (connecte) {
        //console.log("L'ordinateur est connecté à Internet");
        this.wifiok = true;
      } else {
        //console.log("L'ordinateur n'est pas connecté à Internet");
        this.wifiok = false;
      }
    });

  }

  getAllMapsHttp() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/list")
      .subscribe(
        (data) => {
          console.log(data);
          console.log("getAllMapsOK");
          this.all_maps = data;
          //console.log(this.mapdata.Id);
          // if(this.all_maps!=undefined){

          // }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getCurrentLocations() {
    //avoir les poi de la map actuelle
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
      .subscribe(
        (data) => {
          console.log("get locations ok");
          this.all_locations = data;
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getRoundList() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
      .subscribe(
        (data) => {
          this.rounddata = data;
          //console.log(this.rounddata);
          if (this.id_current_map && this.rounddata) {
            this.round_current_map = this.rounddata.filter(
              (round) => round.Map == this.id_current_map
            );
            this.round_map_selected = this.round_current_map;
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getRoundListAutoLaunch(map_id:number) {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
      .subscribe(
        (data) => {
          this.rounddata = data;
          //console.log(this.rounddata);
          if (map_id && this.rounddata) {
            this.round_map_selected = this.rounddata.filter(
              (round) => round.Map == map_id
            );
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  eyesHttp(id: number) {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }

  deleteEyesHttp(id: number) {
    this.httpClient
      .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }

  skipHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/skip", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("skipOK");
        },
        (err) => {
          console.log(err);
          console.log("skipPBM");
        }
      );
  }

  startRoundHttp(idRound: number) {
    // start the round

    this.httpClient
      .put(
        this.httpskomnav + this.param.localhost + "/api/rounds/current/id/" + idRound,
        { observe: "response" }
      )
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("startRoundOK");
        },
        (err) => {
          console.log(err);
          console.log("startRoundPBM");
        }
      );
  }



  startMaskDetection(start:boolean){
    var serviceMaskDetection = new ROSLIB.Service({
      ros : this.ros,
      name : '/toggle_mask_detection_node',
      serviceType : 'std_srvs/SetBool'
    });
  
    var request = new ROSLIB.ServiceRequest({
      data:start,
      
    });
  
    serviceMaskDetection.callService(request, function(result) {
      console.log("*********service mask****************")
      console.log(result);
    });

    if(start){
      this.listener_mask.subscribe((message) => {
        //console.log('Received message on ' + this.listener_mask.name + ': ' + message.data);
        this.maskdetected=message.data;
        
        // if (this.maskdetected){
        //   console.log("alert mask");
        // }
      });
    }else{
      this.listener_mask.unsubscribe();
    }
  }
  
  startPeopleDetection(start:boolean){
    var servicePeopleDetection = new ROSLIB.Service({
      ros : this.ros,
      name : '/toggle_fall_detection_node',
      serviceType : 'std_srvs/SetBool'
    });
  
    var request = new ROSLIB.ServiceRequest({
      data:start,
      
    });
  
    servicePeopleDetection.callService(request, function(result) {
      console.log("*********service peopledetect****************")
      console.log(result);
    });

    if(start){
      this.listener_fall.subscribe((message) => {
        //console.log('Received message on ' + this.listener_fall.name + ': ' + message.data);
        this.fdetected=message.data;
        if(message.data && this.stoplookFall===0 && this.param.patrolInfo.fall_detection==1){
          console.log("fall");
          this.stoplookFall=1;
          this.falldetected=true;
          if(this.stoplookPerson>0){
           this.stoplookPerson=1;
          }else{
            this.personinterv = setInterval(() => this.stopLookPerson(), 1000);
          }
          this.fallinterv = setInterval(() => this.stopLookFall(), 1000);
        }
       
      });

      this.listener_person.subscribe((message) => {
        //console.log('Received message on ' + this.listener_person.name + ': ' + message.data);
        this.pdetected=message.data;
        if(message.data && this.stoplookPerson===0 && this.param.patrolInfo.person_detection==1){
          this.stoplookPerson=1;
          this.persondetected=true;
            
          this.personinterv = setInterval(() => this.stopLookPerson(), 1000);
        }
       
      });
    }else{
      this.listener_person.unsubscribe();
      this.listener_fall.unsubscribe();
    }
  }

  connect() {
    this.is_connected = true;
  }

  connectionLost() {
    this.is_connected = false;
  }

  background() {
    this.is_background = true;
  }

  foreground() {
    this.is_background = false;
  }

  getImgDetectionHttp()  {
    return this.httpClient.get(
      this.httpskomnav + this.param.localhost + "/detections/detection_image.png",
      { responseType: "blob" });
    
    }

  getImgMaskHttp(){
    return this.httpClient.get(
      this.httpskomnav + this.param.localhost + "/detections/mask_detection_image.png",
      { responseType: "blob" });
    
    }
  
}
