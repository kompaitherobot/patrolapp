import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiService } from '../services/api.service';
import { FirstPage } from '../pages/first/first';
import { AlertService } from '../services/alert.service';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = FirstPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public api: ApiService, public alert: AlertService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      platform.pause.subscribe(() => {
        this.api.pauseHttp(false);
        this.api.background();
        window.close();
        
    });

      platform.resume.subscribe(() => {
    });

    });
  }
}

