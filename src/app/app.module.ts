import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ComponentsModule } from '../components/components.module';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FirstPage } from '../pages/first/first';
import { HeadpageComponent } from '../components/headpage/headpage';
import { ApiService } from '../services/api.service';
import { AlertService } from '../services/alert.service';
import { HttpClientModule } from '@angular/common/http';
import { BackgroundMode } from '@ionic-native/background-mode';
import { TutoPage } from '../pages/tuto/tuto';
import { PopupService } from '../services/popup.service';
import { ParamService } from '../services/param.service';
import { SpeechService } from '../services/speech.service';
import { ParamPage } from '../pages/param/param';
import { LanguePage } from '../pages/langue/langue';
import { PasswordPage } from '../pages/password/password';
import { SMSPage } from '../pages/sms/sms';
import { MailPage } from '../pages/mail/mail';
import { PhotoHistoryPage } from '../pages/photoHistory/photoHistory';
import { RobotBehaviourPage } from '../pages/robotBehaviour/robotBehaviour';
import { AutoLaunchPage } from '../pages/autolaunch/autolaunch';


@NgModule({
  declarations: [
    MyApp,
    FirstPage,
    HomePage,
    TutoPage,
    LanguePage,
    PasswordPage,
    MailPage,
    SMSPage,
    ParamPage,
    PhotoHistoryPage,
    RobotBehaviourPage,
    AutoLaunchPage
   
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FirstPage,
    HomePage,
    TutoPage,
    ParamPage,
    LanguePage,
    PasswordPage,
    MailPage,
    SMSPage,
    PhotoHistoryPage,
    RobotBehaviourPage,
    AutoLaunchPage,
    HeadpageComponent
    
  ],
  providers: [
    BackgroundMode,
    ApiService,
    AlertService,
    PopupService,
    ParamService,
    SpeechService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
