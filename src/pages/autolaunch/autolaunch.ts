import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import { ApiService } from '../../services/api.service';
import { ToastController } from 'ionic-angular';
import { Select, Content } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';



@Component({
  selector: 'page-autolaunch',
  templateUrl: 'autolaunch.html'
})

export class AutoLaunchPage implements OnInit {

  selection: any;
  newMission: any = {};
  todayDate: string;
  minDate: string;
  maxDate: string;
  loading: any;
  selectMap: any;
  selectDuration: any;
  selectRound: any;
  selectType: any;
  pickerOptions: any;
  wait: any;
  

  @ViewChild("select1") select1: Select;
  @ViewChild("select2") select2: Select;
  @ViewChild("select3") select3: Select;
  @ViewChild("select4") select4: Select;
  @ViewChild("content1") content1: Content;
  @ViewChild("content2") content2: Content;

  logForm() {
    var lengthMissionList = this.param.missionsList.length;
    this.param.addMission(this.newMission);
    this.param.getDataReservationPatrol(this.api.rounddata);
    this.loading = this.loadingCtrl.create({
    });
    this.loading.present();
    this.wait = setInterval(() => this.checkNewLengthAdd(lengthMissionList),1000);
  }

  ngOnInit() {
    this.param.getDataReservationPatrol(this.api.rounddata);
    this.api.getAllMapsHttp();
  }



  constructor( public param:ParamService, public api:ApiService, public toastCtrl:ToastController, public loadingCtrl: LoadingController) {
    this.selection = "today";
    this.todayDate = new Date().toLocaleDateString(this.param.langage) + " " + new Date().toLocaleTimeString('fr-FR').substring(0,5);
    this.minDate = new Date().toISOString().substring(0,10) + "T" + new Date().toLocaleTimeString();
    this.newMission.date = this.minDate;
    this.newMission.map = this.api.id_current_map;
    this.selectMap = {
      title: this.param.datatext.map,
    };
    this.selectRound = {
      title: this.param.datatext.roundSelection,
    };
    this.selectDuration = {
      title: this.param.datatext.duration,
    };
    this.selectType = {
      title: this.param.datatext.type,
    };
    this.pickerOptions = {
      title: this.param.datatext.date,
    };
  }

  checkNewLengthAdd(oldLength:number){
      this.param.getDataReservationPatrol(this.api.rounddata);
      console.log(this.param.missionsList.length,oldLength);
      if(this.param.missionsList.length > oldLength){
        this.loading.dismiss();
        this.api.addMission = !this.api.addMission;
        this.okToast(this.param.datatext.missionAdded);
        setTimeout(() => {
          if(this.selection == "all")
            this.content1.scrollToBottom(300);
          else
            this.content2.scrollToBottom(300);
        }, 300
        );
        clearInterval(this.wait);
      }
  }

  checkNewLengthDelete(oldLength:number){
    this.param.getDataReservationPatrol(this.api.rounddata);
    if(this.param.missionsList.length < oldLength){
      this.loading.dismiss();
      this.okToast(this.param.datatext.mailRemove);
      clearInterval(this.wait);
    }
}

  removeMission(ev,mission:any){
    ev.preventDefault();
    var oldLength = this.param.missionsList.length;
    this.param.deleteMission(mission.id);
    this.param.getDataReservationPatrol(this.api.rounddata);
    this.loading = this.loadingCtrl.create({
    });
    this.loading.present();
    this.wait = setInterval(() => this.checkNewLengthDelete(oldLength),500);
  }

  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }

  createMission(ev){
    ev.preventDefault();
    this.todayDate = new Date().toLocaleDateString(this.param.langage) + " " + new Date().toLocaleTimeString('fr-FR').substring(0,5);
    this.minDate = new Date().toISOString().substring(0,10) + "T" + new Date().toLocaleTimeString();
    this.newMission.date = this.minDate;
    this.api.addMission = !this.api.addMission;
  }

  updateRoundList(){
    this.newMission.idRound=undefined;
    this.api.getRoundListAutoLaunch(this.newMission.map);
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
  
}
