import { Component, OnInit } from '@angular/core';
import { ParamService } from '../../services/param.service';
import { LoadingController } from 'ionic-angular';
import { PopupService } from '../../services/popup.service';
import { ToastController } from 'ionic-angular';



@Component({
  selector: 'page-photoHistory',
  templateUrl: 'photoHistory.html'
})

export class PhotoHistoryPage implements OnInit {

  buttonColor: any;

  ngOnInit() {
    this.param.getDataPatrol();
    this.popup.onSomethingHappened3(this.deleteHistory.bind(this));
  }

  constructor( public param:ParamService, public loadingCtrl:LoadingController, public popup:PopupService, public toastCtrl: ToastController) {
    if(this.param.patrolInfo.photo_history == 1){
      this.buttonColor = "default";
    } else {
      this.buttonColor = "light";
    }
  }

  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }

  update_photo_history(ev){
    ev.preventDefault();
    if(this.param.patrolInfo.photo_history == 1){
      this.param.patrolInfo.photo_history = 0;
      this.buttonColor = "light";
    } else {
      this.param.patrolInfo.photo_history = 1;
      this.buttonColor = "default";
    }
    this.param.updatePatrol();
  }

  deleteHistory(){
    this.param.deletePhotoHistory();
    this.param.photos.splice(0,this.param.photos.length);
    this.okToast(this.param.datatext.mailRemove);
  }
}
