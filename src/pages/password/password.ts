import { Component, OnInit } from '@angular/core';
import { ParamService } from '../../services/param.service';
import { NavController} from 'ionic-angular';
import { PopupService } from '../../services/popup.service';

@Component({
    selector: 'page-password',
    templateUrl: 'password.html'
})
export class PasswordPage implements OnInit {
  password:any;
  newpassword="";
  warn=false;
  constructor(public navCtrl: NavController,public popup: PopupService,public param:ParamService) { }

  ngOnInit() {
  }

  editpassword(ev){
    ev.preventDefault();
    if(this.password===atob(this.param.robot.password)){
      if(this.newpassword.length>=2){
        this.popup.showToast(this.param.datatext.pswdsaved,3000,"middle");
        this.param.robot.password=btoa(this.newpassword);
        this.param.updateRobot();
        this.password="";
        this.newpassword="";
        this.navCtrl.pop();
      }
      
    }else{
      this.warn=true;
    }
    

  }

}
