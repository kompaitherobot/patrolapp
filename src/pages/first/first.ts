import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ParamService } from "../../services/param.service";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { HomePage } from "../home/home";
import { LoadingController } from "ionic-angular";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";

@Component({
  selector: "page-first",
  templateUrl: "first.html",
})
export class FirstPage implements OnInit {
  homePage = HomePage;
  appopen: any;
  cpt_open: number;
  loading: any;

  constructor(
    public loadingCtrl: LoadingController,
    public popup: PopupService,
    public speech: SpeechService,
    public param: ParamService,
    public alert: AlertService,
    public api: ApiService,
    public navCtrl: NavController
  ) {
    this.cpt_open = 0;
    this.loading = this.loadingCtrl.create({});
    this.loading.present();
  }

  ngOnInit() {
    this.appopen = setInterval(() => this.appOpen(), 500);
    this.speech.getVoice();
  }

  appOpen() {
    if (this.param.localhost != undefined) {
      this.api.checkrobot();
    }
    this.api.checkInternet();
    this.alert.checkwifi(this.api.wifiok);
    this.cpt_open += 1;
    if (this.cpt_open === 15 && !this.api.robotok) {
      this.popup.startFailedAlert();
      this.speech.speak(this.param.datatext.cantstart);
    } else if (this.cpt_open === 15 && this.api.robotok) {
      this.popup.relocAlert();
      this.speech.speak(this.param.datatext.relocAlert_title + " " + this.param.datatext.relocAlert_message);
    }
    if (!this.api.appOpened) {
      this.param.getDataRobot();
      this.param.getDataPatrol();
      this.param.getMail();
      this.param.getBattery();
      this.param.getDurationNS();
      //this.param.getPhone();
     
      if (this.param.robot && this.param.duration && this.param.maillist) {
        this.param.fillData();
        if (this.param.robot.httpskomnav == 0) {
          this.api.httpskomnav = "http://";
          this.api.wsskomnav = "ws://";
        } else {
          this.api.httpskomnav = "https://";
          this.api.wsskomnav = "wss://";
        }
        if (this.param.robot.httpsros == 0) {
          this.api.wssros = "ws://";
        } else {
          this.api.wssros = "wss://";
        }
      }
      if (this.api.robotok && this.param.langage) {
        if (!this.api.socketok)
        {
          this.api.instanciate();
        }
        
        this.api.getCurrentMap();
        this.api.getRoundList();
        this.param.getreservations();
        if (this.api.mapdata) {
          this.api.appOpened = true;
        }
      }
    } else if (this.api.appOpened) {
      
      this.param.getDataPhoto();
      this.api.eyesHttp(23);
      this.alert.appOpen(this.api.mailAddInformationBasic());
      if (this.api.round_current_map) {
        this.api.abortHttp();
        if(this.param.patrolInfo.mask_detection==1){
          this.api.startMaskDetection(true);
        }
        this.api.startPeopleDetection(true);
        this.api.getCurrentLocations();
        this.navCtrl.setRoot(this.homePage);
        this.api.textHeadBand = this.param.datatext.surveillance;
        this.loading.dismiss();
        clearInterval(this.appopen);
        if (this.popup.alert_blocked) {
          this.popup.alert_blocked.dismiss();
        }
        console.log(this.cpt_open);
      } else {
        this.api.getRoundList();
      }
    }
  }
}
