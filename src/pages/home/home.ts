import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import nipplejs from "nipplejs";
import { NavController } from "ionic-angular";
import { Select } from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
declare var ROSLIB: any;

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage implements OnInit {
  size: number = 70;
  maxrad: number = 0.08;
  maxlin: number = 0.12;
  maxback: number = 0.08;
  detection_url = "http://" + this.param.localhost + "/detections/detection_image.png";
  vrad: number;
  vlin: number;
  update: any;
  cptduration: any;
  gamepadinterv: any;
  is_abort: boolean; // to send request abort just one time
  start_from_docking: boolean; //to know when the robot start from docking
  cpt_locked: number; // cpt to know if kompai is blocked
  round_start: boolean;
  sum_blocked: number;
  time_blocked: number;
  cpt_annonce: Date;
  cpt_wait: number;
  date_blocked: Date;
  cpt_round_start: number;
  selectOptions: any;
  selectDuration: any;
  selectDetect: any;

  @ViewChild("imgpersondetect") imgElement: ElementRef;
  @ViewChild("canvas") canvas: ElementRef;
  @ViewChild("canvaspersondetect") canvaspersondetect: ElementRef;
  //@ViewChild("canvasros") canvaros : ElementRef;
  imgRos : any;
  @ViewChild("select1") select1: Select;
  @ViewChild("select2") select2: Select;
  @ViewChild("select3") select3: Select;
  videoWidth = 0;
  videoHeight = 0;
  imgWidth = 0;
  imgHeight = 0;
  imge:any;
  SelectDuration: number = 60;
  SelectDetect: Number = 1;
  SelectRound: Number;
  allowdetect: boolean;
  url: any; //the snapshot
  cam: boolean;
  nbNotifications: number;
  buttonColor: string;
  videoHidden: boolean;
  wait:boolean=false;
  options:any

  currentx: number;
  currenty: number;
  lastx: number;
  lasty: number;
  countjoystick: number;
  checkjoystick: boolean;
  stopjoystick: boolean;
  manager:any

  ngOnInit() {
    this.speech.getVoice();
    this.popup.onSomethingHappened5(this.accesspic.bind(this));
  }

  ionViewWillEnter() {
    if (this.param.patrolInfo.photo_history == 0) {
      this.nbNotifications = 0;
      this.buttonColor = "primary";
    }

    this.renderer.setProperty(
      this.canvas.nativeElement,
      "width",
      640
    );
    this.renderer.setProperty(
      this.canvas.nativeElement,
      "height",
      480
    );

  }
  constructor(
    public navCtrl: NavController,
    public speech: SpeechService,
    public param: ParamService,
    public popup: PopupService,
    public api: ApiService,
    public alert: AlertService,
    private renderer: Renderer2,
    public sanitizer: DomSanitizer
  ) {
    this.cptduration = setInterval(() =>
      this.mail(), 120000);
    this.update = setInterval(() => this.getUpdate(), 500); // update the status and the trajectory every 1/2 secondes

    this.is_abort = true;
    this.start_from_docking = false;
    this.round_start = false;
    this.cpt_locked = 0;
    this.sum_blocked = 0;
    this.time_blocked = 0;
    this.cpt_wait = 0;
    this.allowdetect = true;
    this.cpt_round_start = 0;
    this.currentx = 0 ;
    this.currenty = 0 ;
    this.lastx = 0 ;
    this.lasty = 0 ;
    this.checkjoystick = false;
    this.stopjoystick = false;
    // declenche la popup de permission camera si besoin
    // var de cam

    console.log(this.param.robot.cam_USB);
    // if(this.param.robot.cam_USB==1)
    // {
    //   this.setupconstraint();
    // }

    this.selectOptions = {
      title: this.param.datatext.patrol,
      //subTitle: 'Choisir une ronde'
    };

    this.selectDuration = {
      title: this.param.datatext.patrolduration,
    };
    this.selectDetect = {
      title: this.param.datatext.alertbody,
    };

    this.cam = true;
    this.nbNotifications = 0;
    this.buttonColor = "primary";
    this.videoHidden = false;


    // CAMERA ROS
    // if( this.param.robot.cam_USB==0 ){

      var cameraros =this.api.ros;

      var listener_camera = new ROSLIB.Topic({
        ros : cameraros,
        //name : '/top_webcam/image_raw',
        name : '/fisheye_cam/compressed',
        //name : '/D435_camera_FWD/color/image_raw',

        messageType : 'sensor_msgs/CompressedImage'
      });

      listener_camera.subscribe((message) => {

        //console.log('Received message on ' + listener_camera.name + ': ');
        this.imgRos = document.getElementById('imgcompressed');
        //console.log(message);

        var bufferdata = this.convertDataURIToBinary2(message.data);

        var blob = new Blob([bufferdata], {type: 'image/jpeg'});
        var blobUrl = URL.createObjectURL(blob);

        this.imgRos.src = blobUrl;


      });

    //}

  // CAMERA ROS FIN

  }

  mail(){
    this.param.cptDuration(); //update duration
    if(this.param.durationNS.length>1){
      if(this.api.wifiok){
        this.alert.duration(this.param.durationNS[0],this.api.mailAddInformationBasic());
      }
    }
  }

  convertDataURIToBinary2(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));


    for(var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }


  joystickmove() {
    this.api.joystickHttp(this.vlin, this.vrad);
  }

  joystickstop() {
    this.api.joystickHttp(0, 0);
    
  }

  createjoystick(){
    this.options = {
      zone: document.getElementById("zone_joystick"),
      size: 3 * this.size,
    };

    this.manager = nipplejs.create(this.options);

    this.manager.on("move", (evt, nipple) => {
      this.currentx = nipple.raw.position.x ;
      this.currenty = nipple.raw.position.y ;
      this.checkjoystick = true;
      if(!this.stopjoystick){
        if (nipple.direction && nipple.direction.angle && nipple.angle) {
          //console.log(nipple.direction.angle);
          if (nipple.direction.angle === "left") {
            this.vlin = 0;
            this.vrad = (0.3 * nipple.distance) / 50;
          } else if (nipple.direction.angle === "right") {
            this.vlin = 0;
            this.vrad = -(0.3 * nipple.distance) / 50;
          } else if (nipple.direction.angle === "up") {
            this.vlin = (this.maxlin * nipple.distance) / 50;
            this.vrad = 0;
            // if (Math.abs((nipple.angle.radian - Math.PI / 2) / 2) > 0.18) {
            //   this.vrad = (nipple.angle.radian - Math.PI / 2) / 2 - 0.09;
            // } else {
            // this.vrad = 0;
            // }
          } else if (nipple.direction.angle === "down") {
            this.vlin = -(this.maxback * nipple.distance) / 50;
            this.vrad = 0;
          }
        }
      }else{
        this.vlin = 0;
        this.vrad = 0;
      }
    });

    this.manager.on("added", (evt, nipple) => {
      console.log("added");
      this.gamepadinterv = setInterval(() => {if(!this.stopjoystick){this.joystickmove();}}, 500);
    });
    this.manager.on("end", (evt, nipple) => {
      console.log("end");
      this.vlin = 0;
      this.vrad = 0;
      clearInterval(this.gamepadinterv);
      this.checkjoystick = false;
      this.stopjoystick = false;
      this.joystickstop();
    });
  }

  ngAfterViewInit() {
    console.log(document.getElementById("zone_joystick"));
    this.createjoystick();
   
  }



  ionViewWillLeave() {

    //clearInterval(this.update);
    //clearInterval(this.cptduration);
  }

  btnPressed() {
    // stop the robot if bouton pressed
    if (this.api.is_background || this.api.btnPush || !this.api.is_connected) {
      if (this.api.is_high) {
        this.api.is_high = false;
        if (this.popup.alert_blocked) {
          this.popup.alert_blocked.dismiss();
        }
        this.alert.manualintervention(
          "<br> Blocked for : " +
          this.calcDiffMin(this.date_blocked, new Date()) +
          " <br>" +
          this.api.mailAddInformationRound()
        );
        this.time_blocked = 0;
      }
      this.api.foreground();
      if (this.api.roundActive) {
        this.onGo(false);
      } else if (this.api.towardDocking) {
        this.api.abortNavHttp();
        //this.api.abortDockingHttp();
        this.api.towardDocking = false;
        if (this.api.battery_status.remaining <= this.param.battery.critical) {
          // lowBattery
          this.popup.lowBattery();
        } else {
          this.popup.goDockingConfirm();
        }
      } else if (this.api.towardPOI) {
        this.api.abortNavHttp();
        this.api.towardPOI = false;
      }
      this.api.btnPush = false; // to say that we have received the btn pushed status
    }
  }

  onRoundChange() {
    this.api.abortHttp();
  }

  onDurationChange() {
    this.api.hourStart = new Date();
  }

  updateTrajectory() {

    //console.log(this.api.round_status.status)
    // listen the round status to allow the robot to go to the next
    if (
      this.api.roundActive &&
      !this.api.towardDocking &&
      !this.api.towardPOI
    ) {
      // round in progress
      if (this.api.round_status.status === 2 || this.is_abort) {
        this.is_abort = false;
        this.start_from_docking = false;
      }

      if (this.api.round_status.status === 2) {
        // round start correctly
        this.round_start = true;
        if (!this.api.start) {
          this.api.start = true;
          this.api.hourStart = new Date();
        }
      } else if (this.api.docking_status.status === 3) {
        // start from docking
        this.start_from_docking = true;
      } else if (this.api.round_status.status === 0 && this.round_start) {
        // somebody stop round remotely
        this.alert.remoteStop(this.api.mailAddInformationBasic());
        this.api.roundActive = false;
        this.round_start = false;
        this.is_abort = true;
        this.api.abortHttp();
        this.popup.RemoteConfirm();
      } else if (this.api.round_status.status === 0 && !this.round_start) {
        //round fail
        this.cpt_round_start += 1;
        if (this.cpt_round_start > 10) {
          this.popup.errorNavAlert();
          this.alert.naverror(this.api.mailAddInformationBasic());
          this.api.roundActive = false;
          this.round_start = false;
          this.is_abort = true;
          this.api.abortHttp();
        }
      } else if (this.api.round_status.status === 4) {
        //wait acknowledge
        if (!this.api.btnPush) {
          //verify if it is cause by a btn pushed
          this.api.acknowledgeHttp();
        }
      } else if (this.api.round_status.status === 5) {
        // round completed
        this.startRound();
      } else if (this.api.round_status.status === 6) {
        //hight current
        this.api.roundActive = false;
        this.round_start = false;
        this.is_abort = true;
        this.api.pauseHttp(false);
        this.popup.errorNavAlert();
        this.alert.naverror(this.api.mailAddInformationRound());
        this.api.is_high = true;
        this.date_blocked = new Date();
        this.capture();
      } else if (this.api.navigation_status.status === 5) {
        // lost
        this.popup.lostAlert();
        this.alert.robotLost(this.api.mailAddInformationBasic());
        this.api.roundActive = false;
        this.round_start = false;
        this.is_abort = true;
        this.api.abortHttp();
      }
    }
    //// go docking or go poi in progress
    else if (this.api.towardDocking || this.api.towardPOI) {
      if (this.api.navigation_status.status === 5) {
        // nav error
        this.round_start = false;
        this.api.towardDocking = false;
        this.api.towardPOI = false;
        this.api.abortNavHttp();
        this.popup.errorNavAlert();
        this.alert.naverror(this.api.mailAddInformationBasic());
      } else if (this.api.docking_status.status === 3) {
        this.api.towardDocking = false;
        this.round_start = false;
        this.alert.charging(this.api.mailAddInformationBasic());
        this.api.close_app = true; //close application
        this.api.deleteEyesHttp(23);
        this.api.startPeopleDetection(false);
        this.api.startMaskDetection(false);
        setTimeout(() => {
          window.close();
        }, 1000);
      } else if (
        this.api.towardDocking &&
        this.api.navigation_status.status === 0 &&
        this.api.docking_status.detected
      ) {
        //connect to the docking when he detect it
        this.api.connectHttp();
      } else if (this.api.towardPOI) {
        if (this.api.navigation_status.status === 0) {
          this.cpt_wait += 1;
          if (this.cpt_wait > 1) {
            this.api.towardPOI = false;
            this.api.textHeadBand = this.param.datatext.sentinel.toUpperCase();
            this.cpt_wait = 0;
          }
        }
      }
    } //// manual docking
    else if (this.api.docking_status.status === 3 && !this.api.roundActive) {
      if (!this.is_abort) {
        this.api.abortHttp();
      }
      this.is_abort = true;
      this.api.towardDocking = false;
      this.api.towardPOI = false;
      this.round_start = false;
    }
    //// nothing in progress
    else {
      if (
        this.api.round_status.status === 0 ||
        this.api.round_status.status === 3
      ) {
        this.is_abort = true;
      } else if (!this.is_abort) {
        //this.is_abort=true;
        //this.api.pauseHttp(true);
      }
    }
  }

  getUpdate() {

    //console.log(this.api.round_current_map.filter(r=> r.Id==this.param.reservations.idRound).length>0);
    if (this.param.reservations && this.param.reservations.idApp == 3 && this.calcDiffMin(new Date(), new Date(this.param.reservations.date)) === 0 && !this.param.autolaunch) {
      //console.log("auto launch");

      if (this.api.round_current_map.filter(r => r.Id == this.param.reservations.idRound).length > 0) {
        var rname = this.api.round_current_map.filter(r => r.Id == this.param.reservations.idRound)[0].Name;
        this.param.autolaunch = true;
        this.api.hourStart = new Date();
        this.SelectRound = Number(this.param.reservations.idRound);
        this.SelectDuration = Number(this.param.reservations.tpsReserv);

        if (this.api.roundActive && this.api.round_status.round && this.api.round_status.round.Id == this.param.reservations.idRound) {
          console.log("do nothing");
        } else {
          this.speech.speak(this.param.datatext.autolaunch + rname);
          this.popup.showToast(this.param.datatext.autolaunch + rname, 15000, "middle")
          if (this.api.roundActive) {
            this.onGo(false);

          }
          this.api.abortHttp();
          if (this.api.anticollision_status.forward == 2) {

            this.api.disconnectHttp();
          }

          setTimeout(() => {

            this.onGo(false);
          }, 8000);
        }


      }
    }

    // update trajectory, butons input, round status
    if (this.api.close_app) {
      clearInterval(this.update);
      clearInterval(this.cptduration);
    }

    // if (this.api.io_status.dIn && this.api.io_status.dIn[13] && this.param.patrolInfo.fall_detection == 1) {
    //   this.popup.showToastRed(this.param.datatext.FallConfirm_title, 500, "bottom");
    // }
    // else if (this.api.io_status.dIn && this.api.io_status.dIn[12] && this.param.patrolInfo.person_detection == 1) {
    //   this.popup.showToastRed(this.param.datatext.AlertPersonConfirm, 500, "bottom");
    // }
    if (this.api.fdetected && this.param.patrolInfo.fall_detection == 1) {
      this.popup.showToastRed(this.param.datatext.FallConfirm_title, 500, "bottom");
    }
    else if (this.api.pdetected && this.param.patrolInfo.person_detection == 1) {
      this.popup.showToastRed(this.param.datatext.AlertPersonConfirm, 500, "bottom");
    }

    if(this.api.maskdetected && this.param.patrolInfo.mask_detection == 1){
      this.popup.showToastRed(this.param.datatext.pleasemask, 500, "bottom");
      if(!this.wait){
        this.wait =true;
        this.speech.speak(this.param.datatext.speechmask);
        this.capturedetect(3);
        setTimeout(() => {
          this.wait =false;
        }, 8000);
      }

    }

    if (this.api.falldetected) {
      // detect a fall

      //console.log(this.api.img_detection);
      console.log("getimgfallOK");


      this.api.falldetected = false;
      this.api.persondetected = false;

      this.round_start = false;
      this.is_abort = true;
      this.api.pauseHttp(false);
      if (!this.api.popupFall) {
        if (this.popup.alert_person) {
          this.api.popupPerson = false;
          this.popup.alert_person.dismiss();
        }
        this.api.popupFall = true;
        this.popup.FallConfirm();
      }

      this.capturedetect(2);
      this.alert.fall(this.api.mailAddInformationBasic());

    }
    else if (this.api.persondetected) {
      // detect a person

      //console.log(this.api.img_detection);
      console.log("getimgpersonOK");


      this.api.persondetected = false;

      if (this.param.patrolInfo.detection_stop == 1) {
        this.round_start = false;
        this.is_abort = true;
        this.api.pauseHttp(false);
      }

      if (!this.api.popupPerson && !this.api.popupFall) {
        this.api.popupPerson = true;
        this.popup.PersonConfirm();
      }
      this.capturedetect(1);
      this.alert.person(this.api.mailAddInformationBasic());
    }
    if (!this.api.is_connected) {
      this.api.roundActive = false;
      this.round_start = false;
      this.api.towardPOI = false;
      this.api.towardDocking = false;
    } else {
      if (this.api.fct_onGo) {
        this.api.fct_onGo = false;
        this.onGo(false);
      }
      if (this.api.fct_startRound) {
        this.api.fct_startRound = false;
        this.startRound();
      }
      this.btnPressed();
      this.updateTrajectory();
      this.watchIfLocked();
      this.updateTimeRound();
    }


    // test joystick count
    if(this.checkjoystick){

    
      if(this.currentx == this.lastx ){
        if(this.currenty == this.lasty)
        {
          this.countjoystick = this.countjoystick + 1;
          if(this.countjoystick >= 5)
          {
            console.log("stopjoystick");
            this.joystickstop();
            this.stopjoystick = true;
          }
          if(this.countjoystick >= 15)
          {
            clearInterval(this.gamepadinterv);
            this.checkjoystick = false;
            this.manager.destroy();
            this.createjoystick();
            this.checkjoystick=false;
          }
        }
        else
        {
          this.countjoystick = 0;
          this.stopjoystick = false;
          this.checkjoystick = true;
          this.lastx = this.currentx;
          this.lasty = this.currenty;
        }
      }
      else
      {
        this.countjoystick = 0;
        this.stopjoystick = false;
        this.checkjoystick = true;
        this.lastx = this.currentx;
        this.lasty = this.currenty;
      }
    
    }

  }



  watchIfLocked() {
    if (this.api.roundActive || this.api.towardDocking || this.api.towardPOI) {
      if (
        (this.api.towardDocking && this.api.docking_status.detected) ||
        this.api.docking_status.status === 3 ||
        (this.api.anticollision_status.forward < 2 &&
          this.api.anticollision_status.right < 2 &&
          this.api.anticollision_status.left < 2)
      ) {
        this.cpt_locked = 0;
        if (this.api.roundActive) {
          //patrol
          if (this.api.is_blocked) {
            this.time_blocked = this.calcDiffMin(this.date_blocked, new Date());
            this.sum_blocked += this.time_blocked;
            this.alert.noLongerBlocked(
              "<br> Blocked for : " +
              this.time_blocked +
              "<br>" +
              this.api.mailAddInformationRound()
            );
            this.api.is_blocked = false;
            this.api.resumeHttp();
            if (this.popup.alert_blocked) {
              this.popup.alert_blocked.dismiss();
            }
            this.time_blocked = 0;
          }
        } else {
          //simple nav
          if (this.api.is_blocked) {
            this.api.is_blocked = false;
            if (this.popup.alert_blocked) {
              this.popup.alert_blocked.dismiss();
            }
          }
        }
      } else if (
        this.api.anticollision_status.forward === 2 ||
        this.api.anticollision_status.right === 2 ||
        this.api.anticollision_status.left === 2
      ) {
        this.cpt_locked += 1;
      }

      if (this.cpt_locked > 20 && !this.api.is_blocked) {
        this.capture();
        if (this.api.roundActive) {
          this.date_blocked = new Date();
          //this.api.pauseHttp(true);
          this.api.skipHttp();
          this.popup.blockedAlert();
          this.api.is_blocked = true;
          this.alert.blocking(this.api.mailAddInformationRound());
        } else if (this.api.towardDocking) {
          this.popup.blockedAlert();
          this.api.is_blocked = true;
          this.alert.blockingdocking(this.api.mailAddInformationBasic());
        } else if (this.api.towardPOI) {
          this.popup.blockedAlert();
          this.api.is_blocked = true;
        }
      } else if (this.cpt_locked === 30 && this.api.round_status.status === 5) {
        this.startRound();
      }
    }
  }

  startRound() {
    //start the right round
    if (this.SelectRound) {
      this.api.startRoundHttp(Number(this.SelectRound));
    }
  }

  calcDiffMin(one: Date, two: Date) {
    var DateBegMin = one.getMinutes();
    var DateBegHours = one.getHours() * 60;
    var DateBeg = DateBegMin + DateBegHours;
    var DateEndMin = two.getMinutes();
    var DateEndHours = two.getHours() * 60;
    return DateEndMin + DateEndHours - DateBeg;
  }

  updateTimeRound() {
    if (this.api.start) {
      if (this.calcDiffMin(this.api.hourStart, new Date()) > this.SelectDuration) {
        // after 60 min of round the robot must go to the docking
        this.alert.roundCompleted(
          this.api.mailAddInformationRound() +
          "<br> Total blocking : " +
          this.sum_blocked +
          " <br>"
        );
        this.api.abortHttp();
        this.api.roundActive = false;
        this.api.towardDocking = true;
        this.sum_blocked = 0;
        this.time_blocked = 0;
        this.api.hourStart=new Date();
        setTimeout(() => {
          this.api.reachHttp("docking");
        }, 2000);
      }
    }
  }

  onclickGo(ev) {
    ev.preventDefault();
    this.onGo(false);
  }

  onGo(btnpressed: boolean) {
    // buton go code
    this.cpt_locked = 0;
    this.cpt_round_start = 0;
    if (this.api.is_high) {
      this.api.is_high = false;
      if (this.popup.alert_blocked) {
        this.popup.alert_blocked.dismiss();
      }
      this.alert.manualintervention(
        "<br> Blocked for :" +
        this.calcDiffMin(this.date_blocked, new Date()) +
        "<br>" +
        this.api.mailAddInformationRound()
      );
      this.time_blocked = 0;
    }
    if (this.api.towardPOI) {
      // stop go poi
      this.api.textHeadBand = this.param.datatext.sentinel.toUpperCase();
      this.round_start = false;
      this.api.towardPOI = false;
      this.api.abortNavHttp();
    } else if (this.api.towardDocking) {
      // stop go docking
      this.round_start = false;
      this.api.towardDocking = false;
      this.api.abortNavHttp();
      //this.api.abortDockingHttp();
      if (this.api.battery_status.remaining <= this.param.battery.critical) {
        // lowBattery
        this.popup.lowBattery();
      } else {
        this.popup.goDockingConfirm();
      }
    } else if (!this.api.roundActive && !this.api.towardDocking) {
      // run round
      this.round_start = false;
      if (this.api.anticollision_status.forward === 2 && this.api.battery_status.status != 0 && !this.param.autolaunch) {
        this.popup.StartBlockedAlert();
      }
      else if (
        !(this.api.docking_status.status === 3) &&
        this.api.battery_status.remaining <= this.param.battery.critical
      ) {
        // lowBattery
        this.popup.lowBattery();
      } else if (this.api.docking_status.status === 3) {
        // to know if the robot is in the docking
        if (this.api.battery_status.remaining <= this.param.battery.critical) {
          this.popup.robotmuststayondocking();
        } else {
          if (this.param.autolaunch) {
            this.api.roundActive = true;
            this.api.fct_startRound = true;
          } else {
            this.popup.presentAlert();
          }

        } // pop up docking
      } else {
        if (
          this.api.round_status.status === 0 ||
          this.api.round_status.status === 5
        ) {
          //ready to execute a round
          this.api.roundActive = true;
          this.startRound();

          if (!this.api.start) {
            this.api.hourStart = new Date();
            this.sum_blocked = 0;
            this.time_blocked = 0;
            this.api.start = true;
          }

          setTimeout(() => {
            this.alert.roundLaunched(this.api.mailAddInformationRound());
          }, 2000);

        } else if (this.api.round_status.status === 3) {
          this.api.roundActive = true;
          this.api.resumeHttp();
          setTimeout(() => {
            this.alert.roundLaunched(this.api.mailAddInformationRound());
          }, 2000);
        } else {
          this.api.roundActive = false;
          this.round_start = false;
          this.start_from_docking = false;
          //this.is_abort=true;
          this.api.abortHttp();
          this.alert.roundFailed(this.api.mailAddInformationBasic());
          this.popup.errorlaunchAlert();
        }
      }
    } else {
      //stop round
      if (this.api.is_blocked) {
        this.api.roundActive = false;
        this.api.is_blocked = false;
        if (this.popup.alert_blocked) {
          this.popup.alert_blocked.dismiss();
        }
        this.api.pauseHttp(true);
        this.alert.manualintervention(
          "<br> Blocked for : " +
          this.calcDiffMin(this.date_blocked, new Date()) +
          " <br>" +
          this.api.mailAddInformationRound()
        );
        this.time_blocked = 0;
      } else {
        this.api.pauseHttp(false);
        this.alert.roundPaused(this.api.mailAddInformationRound());
      }

      this.round_start = false;
      if (this.start_from_docking) {
        this.api.abortNavHttp();
        //this.api.abortDockingHttp();
        this.start_from_docking = false;
      }
      if (btnpressed) {
        this.popup.presentConfirm(); //pop up round paused
      }
    }
  }

  // function take snapshot and send mail
  // permettre le snapshot de la video USB ou le canvas ROS
  capture() {
    if (this.param.robot.send_pic == 1) {
  
        //console.log(this.imgRos.width,this.imgRos.height)
        // capture via canvas Ros

        this.canvas.nativeElement
          .getContext("2d")
          .drawImage(this.imgRos, 0, 0);
        var url = this.canvas.nativeElement.toDataURL();

        this.alert.Blocked_c(url);
      

    } else {
      this.alert.Block_c();

    }

  }

  capturedetect(capturewhat: number) { //1 person 2 fall 3mask
    if (this.param.patrolInfo.photo_history == 1) {
      this.nbNotifications++;
      if (this.cam) {
        this.buttonColor = "warning";
      }
    }

    if(this.imgHeight==0 || this.imgWidth ==0){
      this.imge = document.getElementById("imgpersondetect") as HTMLImageElement;
      this.imgHeight = this.imge.naturalHeight;
      this.imgWidth = this.imge.naturalWidth;
      console.log(this.imgHeight);
      console.log(this.imgWidth);

    }

    if(capturewhat===3){
      this.imge.src = "http://" + this.param.localhost + "/detections/mask_detection_image.png?t=" + new Date().getTime();
    }else{
      this.imge.src = "http://" + this.param.localhost + "/detections/detection_image.png?t=" + new Date().getTime();
    }


    setTimeout(() => {

      this.renderer.setProperty(
        this.canvaspersondetect.nativeElement,
        "width",
        this.imgWidth
      );
      this.renderer.setProperty(
        this.canvaspersondetect.nativeElement,
        "height",
        this.imgHeight
      );

      this.canvaspersondetect.nativeElement
        .getContext("2d")
        .drawImage(this.imge, 0, 0);
      this.url = this.canvaspersondetect.nativeElement.toDataURL();
      if (this.param.patrolInfo.photo_history == 1) {
        this.param.photos.splice(0, 0, { url_photo: this.url, date_photo: new Date().toLocaleDateString(this.param.langage), time_photo: new Date().toLocaleTimeString(this.param.langage, { hour: '2-digit', minute: '2-digit' }) });
        this.param.addPhoto(this.url);
      }

      if (capturewhat===2) {
        if (this.param.robot.send_pic == 1) {
          this.alert.PicFall_c(this.url);
        } else {
          this.alert.Fall_c();
        }
      } else if (capturewhat===1) {
        if (this.param.robot.send_pic == 1) {
          this.alert.PicPerson_c(this.url);
        } else {
          this.alert.Person_c();
        }
      }
    }, 3500)

  }

  accesspic(){
    this.videoHidden = true;
    this.nbNotifications = 0;
    this.cam = false;
    this.buttonColor = "primary";
    let shand = document.getElementsByClassName(
      "youtube"
    ) as HTMLCollectionOf<HTMLElement>;

    if (shand.length != 0) {
      shand[0].style.display = "inline";

    }

    let shend = document.getElementsByClassName(
      "video"
    ) as HTMLCollectionOf<HTMLElement>;

    if (shend.length != 0) {
      shend[0].style.display = "none";
    }
  }

  displaycam(ev) {
    ev.preventDefault();
    if (this.cam) {

     if(this.param.patrolInfo.pwd_to_access_pictures==1){
      this.popup.askpswd("seepic");
     }
     else{
      this.accesspic();
     }

    } else {
      this.cam = true;
      this.videoHidden = false;
      this.nbNotifications = 0;
      let shand = document.getElementsByClassName(
        "youtube"
      ) as HTMLCollectionOf<HTMLElement>;

      if (shand.length != 0) {
        shand[0].style.display = "none";
      }

      let shend = document.getElementsByClassName(
        "video"
      ) as HTMLCollectionOf<HTMLElement>;

      if (shend.length != 0) {
        shend[0].style.display = "inline";
      }
    }

  }

  onSliderRelease(ev, id: Select) {
    ev.preventDefault();
    id.open();
  }

}
