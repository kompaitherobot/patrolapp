import { Component, Input, OnInit } from '@angular/core';
import { HomePage } from '../../pages/home/home';
import { TutoPage } from '../../pages/tuto/tuto';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../services/api.service'
import { AlertService } from '../../services/alert.service';
import { PopupService } from '../../services/popup.service';
import { SpeechService } from '../../services/speech.service';
import { ParamService } from '../../services/param.service';
import { ParamPage } from '../../pages/param/param';
import { MailPage } from '../../pages/mail/mail';
import { PhotoHistoryPage } from '../../pages/photoHistory/photoHistory';
import { AutoLaunchPage } from '../../pages/autolaunch/autolaunch';
import { ToastController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'headpage',
  templateUrl: 'headpage.html'
})

export class HeadpageComponent implements OnInit {
  homePage = HomePage;
  tutoPage = TutoPage;
  paramPage = ParamPage;
  mailPage = MailPage;
  historyPage = PhotoHistoryPage;
  autolaunchPage = AutoLaunchPage;
  lowbattery: any;
  cpt_battery: number;
  cpt_open: number;
  microon: boolean;
  cptsos: number;
  update: any;
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  textHeadBand: string;
  volumeState: number; //0 mute,  1 low,  2 hight
  popup_battery: boolean;
  source: any;

  @Input() pageName: string;
  constructor(public toastCtrl: ToastController, public navCtrl: NavController, public api: ApiService, public alert: AlertService, public param: ParamService, public speech: SpeechService, public popup: PopupService, public sanitizer: DomSanitizer) {
    this.microon = false;
    this.volumeState = 1;
    this.cpt_battery = 0;
    this.cpt_open = 0;
    this.cptsos = 0;

  }

  ngOnDestroy(): void {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }

  SOSsend() {
    if (this.cptsos === 1) {
      this.alert.SOS_c();
      this.alert.SOS(this.api.mailAddInformationBasic());
    }
    if (this.api.sos_pressed) {
      this.cptsos += 1;
    }
    if (this.cptsos === 15) {
      this.api.sos_pressed = false;
      this.cptsos = 0;
    }
  }

  updateBattery() {
    if (this.api.statusRobot === 2) {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }
    if (this.api.battery_status.status < 2) {
      //charging
      this.api.batteryState = 4;

    } else if (this.api.battery_status.status === 3 || this.api.battery_status.remaining <= this.param.battery.critical) {
      //critical
      this.api.batteryState = 0;

    } else if (this.api.battery_status.remaining > this.param.battery.high) {
      //hight
      this.api.batteryState = 3;
    } else if (this.api.battery_status.remaining > this.param.battery.low) {
      //mean
      this.api.batteryState = 2;
    } else if (this.api.battery_status.remaining <= this.param.battery.low) {
      //low
      this.api.batteryState = 1;
    } else {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }

  }

  ionViewWillLeave() {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }

  ngOnInit() {
    this.popup.onSomethingHappened1(this.accessparam.bind(this));
    this.popup.onSomethingHappened4(this.accessautolaunch.bind(this));
    // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on

    if (this.pageName === "home") {
      this.update = setInterval(() => this.getUpdate(), 500);//update hour, battery every 1/2 secondes
      this.lowbattery = setInterval(() => this.monitoringBattery(), 60000);
      this.source = setInterval(() => this.api.jetsonOK(), 2000);
      this.textBoutonHeader = this.param.datatext.quit;
      this.textHeadBand = this.param.datatext.surveillance;
      //this.textHeadBand=this.param.datatext.patrol;
    } else if (this.pageName === "param") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.param;
    } else if (this.pageName === "sms") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.receivesms;
    } else if (this.pageName === "mail") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.receivemail;
    } else if (this.pageName === "langue") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.changelangage;
    } else if (this.pageName === "password") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.editpswd;
    } else if (this.pageName === "history") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.photoHistorySettings;
    } else if (this.pageName === "behaviour") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.robotBehaviour;
    } else if (this.pageName === "autolaunch") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.autolaunchHeadPage;
    }
    else {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.tutorial;
    }
  }


  onTouchStatus(ev) {
    ev.preventDefault();

    if (this.api.statusRobot === 2)// if status red
    {
      this.popup.statusRedPresent();
    } else {
      this.okToast(this.param.datatext.statusGreenPresent_message);
    }

  }

  monitoringBattery() {

    if (this.api.batteryState === 0) {//critical
      this.alert.lowBattery(this.api.mailAddInformationBasic());
      this.alert.Battery_c();
      this.popup.lowBattery();
      if (!this.api.towardDocking) {
        this.api.roundActive = false;
        this.api.towardPOI = false;
        this.api.abortNavHttp();
      }



      // setTimeout(
      //   () => {
      //     var docking = this.api.all_locations.filter(
      //       (poi) => poi.Name == "docking"
      //     );
      //     if (docking.length > 0) {
      //       this.api.towardDocking = true;
      //       this.api.reachHttp("docking");
      //       this.alert.askCharge(this.api.mailAddInformationBasic());

      //     } else {
           
      //     }
      //     clearInterval(this.lowbattery);
      //   }, 2000
      // );

    }
  }

  accessparam() {
    this.navCtrl.push(this.paramPage);
  }

  accessautolaunch() {
    this.navCtrl.push(this.autolaunchPage);
  }

  onTouchParam(ev) {
    ev.preventDefault();
    //this.speech.speak("Bonjour les amis. Comment ça va aujourd'hui ! ");
    if (!this.api.roundActive && !this.api.towardDocking && !this.api.towardPOI) {//robot is not moving
      //this.alert.displayParam(this.api.mailAddInformationBasic());
      if (this.pageName === "param" || this.pageName === "sms" || this.pageName === "mail" || this.pageName === "langue" || this.pageName === "password" || this.pageName === "history" || this.pageName === "behaviour")
        console.log("already");
      else if (this.pageName === "autolaunch")
        this.accessparam();
      else {
        //this.navCtrl.push(this.paramPage);
        this.popup.askpswd("param");
      }
    }
  }


  okToast(m: string) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: 3000,
      position: 'middle',
      cssClass: "toastok"
    });
    toast.present();
  }

  onTouchBattery(ev) {
    //this.api.startMaskDetection(false);
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking && !this.api.towardPOI) {
      if (this.pageName === "home") {
        if (this.api.docking_status.status != 3) { // if not on docking
          if (this.api.batteryState === 0) {
            this.popup.lowBattery();
          }
          else {
            this.popup.goDockingConfirm();
          }

        }
        if (this.api.docking_status.status === 3) { //if on docking
          this.popup.leaveDockingConfirm();
        }
      } else {
        this.clicOnMenu();
      }
    } else {
      this.okToast(this.param.datatext.battery + " : " + this.api.battery_status.remaining + "%")
    }
  }


  onTouchWifi(ev) {
    ev.preventDefault();
    //this.api.startMaskDetection(true);
    if (!this.api.wifiok) {
      this.popup.showToastRed(this.param.datatext.nointernet, 3000, "middle");
    } else {
      this.okToast(this.param.datatext.AlertConnectedToI);
    }
  }

  onTouchVolume(ev) {
    ev.preventDefault();
    // var options = {hour:'numeric',minute:'numeric'};
    // var now = new Date().toLocaleString(this.param.langage,options);
    if (this.volumeState === 0) {
      this.volumeState = 1;
      this.param.allowspeech = true;
    } else {
      this.volumeState = 0;
      this.param.allowspeech = false;
    }
  }

  getUpdate() {
    this.SOSsend();
    //update hour, battery
    this.updateBattery();
    this.updateBand();
    var now = new Date().toLocaleString('fr-FR', { hour: 'numeric', minute: 'numeric' });
    this.api.hour = now;
    //console.log(this.batteryState);

    if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
      this.cpt_battery += 1;
    }
    else {
      this.cpt_battery = 0;
    }

    if (this.cpt_battery > 10) {
      this.api.statusRobot = 2;
    }

  }

  updateBand() {
    if (this.api.is_blocked || this.api.is_high) {
      this.textHeadBand = this.param.datatext.errorBlocked_title.toUpperCase();
    }
    else if (this.api.towardDocking) {
      this.textHeadBand = this.param.datatext.godocking;
    }

    else if (this.api.roundActive) {
      //this.textHeadBand=this.param.datatext.patrolInProgress;
      this.textHeadBand = this.param.datatext.patrolling.toUpperCase();
    }
    else if (this.pageName === "home") {
      this.textHeadBand = this.api.textHeadBand;
    }
  }

  onTouchAutoLaunch(ev) {
    ev.preventDefault();
    if (this.pageName == "autolaunch")
      console.log("already");
    else if (this.pageName === "param" || this.pageName === "sms" || this.pageName === "mail" || this.pageName === "langue" || this.pageName === "password" || this.pageName === "behaviour" || this.pageName === "history")
      this.accessautolaunch();
    else
      this.popup.askpswd("autolaunch");
  }


  onTouchHelp(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking && !this.api.towardPOI) {//robot is not moving

      if (!(this.pageName === "tuto")) {
        this.alert.displayTuto(this.api.mailAddInformationBasic());
        this.navCtrl.push(this.tutoPage);
      }

    }
  }

  onquit(ev) {
    ev.preventDefault();
    this.clicOnMenu();
  }

  clicOnMenu() {
    if (this.pageName === 'tuto' || this.pageName === 'param' || (this.pageName === 'autolaunch' && !this.api.addMission)) {
      //return to home page
      this.navCtrl.popToRoot();
    }
    else if (this.pageName === 'sms' || this.pageName === 'mail' || this.pageName === 'langue' || this.pageName === 'password' || this.pageName === 'history' || this.pageName === 'behaviour') {
      this.navCtrl.pop();
    }
    else if (this.pageName === 'autolaunch' && this.api.addMission)
      this.api.addMission = !this.api.addMission;
    else {
      if (this.api.roundActive || this.api.towardDocking || this.api.towardPOI) {
        this.api.abortNavHttp();
        this.api.roundActive = false;
        this.api.towardDocking = false;
        this.api.towardPOI = false;
        this.popup.quitConfirm();
      }
      else {
        this.api.close_app = true;
        this.alert.appClosed(this.api.mailAddInformationBasic());
        //stop the round and quit the app
        this.api.abortHttp();
        this.api.deleteEyesHttp(23);
        this.api.startPeopleDetection(false);
        this.api.startMaskDetection(false);
        setTimeout(
          () => {
            window.close();
          }, 1000
        );
      }
    }
  }
}
